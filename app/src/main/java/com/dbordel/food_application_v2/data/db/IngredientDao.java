package com.dbordel.food_application_v2.data.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.dbordel.food_application_v2.data.db.model.IngredientEntity;

import java.util.List;

@Dao
public interface IngredientDao {

    @Insert
    void insert(IngredientEntity ingredient);

    @Update
    void update(IngredientEntity ingredient);

    @Query("DELETE from ingredient")
    void deleteAllIngrdients();

    @Query("DELETE from ingredient where ingredientId = :ingredientId")
    void deleteIngredient(int ingredientId);

    @Query("SELECT * from ingredient ORDER BY ingredientName ASC")
    LiveData<List<IngredientEntity>> getAllIngredients();

    @Query("SELECT * from ingredient WHERE ingredientName = :ingredientName")
    List<IngredientEntity> getIngredientByName(String ingredientName);

    @Query("SELECT * from ingredient WHERE ingredientId = :ingredientId")
    IngredientEntity getIngredientById(int ingredientId);
}
