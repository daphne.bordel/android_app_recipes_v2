package com.dbordel.food_application_v2.data.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import com.dbordel.food_application_v2.data.db.model.RoomRecipeIngredient;

import java.util.List;

@Dao
public interface RecipeIngredientsDao {

    @Query("SELECT * from recipe_ingredient WHERE recipeId = :id")
    public LiveData<List<RoomRecipeIngredient>> getAllRoomRecipeIngredientByRecipeId(int id);

    @Query("SELECT * from recipe_ingredient WHERE recipeId = :id")
    public List<RoomRecipeIngredient> getIngredientsByRecipeId(int id);

    @Query("SELECT * from recipe_ingredient")
    public LiveData<List<RoomRecipeIngredient>> getRecipesIngredients();

    @Insert
    void insert(RoomRecipeIngredient recipeIngredient);

    @Query("DELETE from recipe_ingredient WHERE id = :id")
    void delete(int id);


}
