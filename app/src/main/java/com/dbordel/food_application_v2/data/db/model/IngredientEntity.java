package com.dbordel.food_application_v2.data.db.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

// INGREDIENT ENTITY
// Name

@Entity(tableName = "ingredient")
public final class IngredientEntity {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name="ingredientId")
        private int ingredientId;

        @ColumnInfo(name="ingredientName")
        private String ingredientName;

        public IngredientEntity(String ingredientName) {

                this.ingredientName = ingredientName;
        }

        public int getIngredientId() {

                return this.ingredientId;
        }

        public void setIngredientId(int ingredientId) {

                this.ingredientId = ingredientId;
        }

        public String getIngredientName() {

                return this.ingredientName;
        }

        public void setIngredientName(String ingredientName) {
                this.ingredientName = ingredientName;
        }
}
