package com.dbordel.food_application_v2.data.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dbordel.food_application_v2.data.db.model.RecipeEntity;

import java.util.List;

@Dao
public interface RecipeDao {

    @Insert
    void insert(RecipeEntity recipe);

    @Query("SELECT * FROM recipe ORDER BY recipeName ASC")
    LiveData<List<RecipeEntity>> getAllRecipes();

    @Query("SELECT * FROM recipe WHERE recipeId = :recipeId")
    RecipeEntity getRecipeById(int recipeId);

    @Query("SELECT * FROM recipe WHERE recipeName = :recipeName")
    RecipeEntity getRecipeByName(String recipeName);

    @Query("SELECT COUNT(*) FROM recipe")
    int countAllRecipes();

    @Update
    int updateRecipe(RecipeEntity recipe);

    @Delete
    void delete(RecipeEntity recipe);
}
