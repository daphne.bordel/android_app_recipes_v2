package com.dbordel.food_application_v2.data.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.dbordel.food_application_v2.data.db.model.IngredientEntity;
import com.dbordel.food_application_v2.data.db.model.RecipeEntity;
import com.dbordel.food_application_v2.data.db.model.RoomRecipeIngredient;

@Database(entities = {RecipeEntity.class, IngredientEntity.class, RoomRecipeIngredient.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE = null;

    //define all Data Access Object here
    public abstract RecipeDao getRecipeDao();
    public abstract  IngredientDao getIngredientDao();
    public abstract RecipeIngredientsDao getRecipeIngredientsDao();

    public static synchronized AppDatabase getDatabaseInstance(Context context){
        if (INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "my_recipes_database").allowMainThreadQueries().build();
        }
        return INSTANCE;
    }

}
