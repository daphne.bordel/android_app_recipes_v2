package com.dbordel.food_application_v2.data.db.model;

//MEASURES FOR INGREDIENTS OF RECIPE
public enum EnumCategoryIngredients {
    VIANDES_POISSONS_OEUFS("Viandes, poissons, oeufs",0),
    PRODUITS_LAITIERS("Produits laitiers",1),
    MATIERES_GRASSES("Matières grasses",2),
    LEGUMES_ET_FRUITS("Légumes et fruits",3),
    CEREALES_LEGUMINEUSES("Céréales et dérivés - légumineuses",4),
    SUCRES_ET_PRODUITS_SUCRES("Sucres et produits sucrés",5),
    BOISSONS("boissons",6);

    private String stringValue;
    private int intValue;

    EnumCategoryIngredients(String l, int i) {
        this.stringValue = l;
        this.intValue = i;
    }

    @Override
    public String toString(){
        return stringValue;
    }
}