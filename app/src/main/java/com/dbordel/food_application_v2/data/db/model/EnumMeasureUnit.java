package com.dbordel.food_application_v2.data.db.model;

//MEASURES FOR INGREDIENTS OF RECIPE
public enum EnumMeasureUnit {
    UNIT("unité(s)",0),
    LITRE("L",1),
    CENTILITRE("cL",2),
    GRAMME("g",3),
    KILO("kg",4),
    PINCEE("pincée(s)",5);

    private String stringValue;
    private int intValue;

    EnumMeasureUnit(String l, int i) {
        this.stringValue = l;
        this.intValue = i;
    }

    @Override
    public String toString(){
        return stringValue;
    }
}
