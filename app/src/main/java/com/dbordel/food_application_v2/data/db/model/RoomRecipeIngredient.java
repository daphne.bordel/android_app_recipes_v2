package com.dbordel.food_application_v2.data.db.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;


// TABLE TO DEFINE RELATION BETWEEN RECIPE AND INGREDIENT
// RecipeId, IngredientId, Quantity, Measure

@Entity(tableName = "recipe_ingredient",
        indices = { @Index(value = { "ingredientId" }) },
        foreignKeys = { @ForeignKey(entity = IngredientEntity.class, parentColumns = "ingredientId", childColumns = "ingredientId"),
                @ForeignKey(entity = RecipeEntity.class, parentColumns = "recipeId", childColumns = "recipeId") })
public final class RoomRecipeIngredient {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id")
    private int id;

    private int ingredientId;
    private int recipeId;
    private String quantity;
    private String mMeasureUnit;

    public RoomRecipeIngredient(int ingredientId, int recipeId, String quantity, String measureUnit) {
        this.ingredientId = ingredientId;
        this.recipeId = recipeId;
        this.quantity = quantity;
        this.mMeasureUnit = measureUnit;
    }

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }

    public int getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(int ingredientId) {
        this.ingredientId = ingredientId;
    }

    public int getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getMeasureUnit() {
        return mMeasureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        mMeasureUnit = measureUnit;
    }
}