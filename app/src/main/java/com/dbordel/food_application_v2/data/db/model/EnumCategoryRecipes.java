package com.dbordel.food_application_v2.data.db.model;

//CATEGORY FOR RECIPE
public enum EnumCategoryRecipes {
    ENTREES("Entrée",0),
    PLATS("Plat",1),
    DESSERTS("Dessert",2);

    private String stringValue;
    private int intValue;

    EnumCategoryRecipes(String l, int i) {
        this.stringValue = l;
        this.intValue = i;
    }

    @Override
    public String toString(){
        return stringValue;
    }
}
