package com.dbordel.food_application_v2.data.db.model;

import android.graphics.drawable.Drawable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

// RECIPE ENTITY
// name, description, timePreparation, timeCooking

@Entity(tableName = "recipe")
public final class RecipeEntity {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="recipeId")
    public int recipeId;

    @ColumnInfo(name="recipeName")
    private String recipeName;
    @ColumnInfo(name="recipeDescription")
    private String recipeDescription;
    @ColumnInfo(name="recipeTimePreparation")
    private String recipeTimePreparation;
    @ColumnInfo(name="recipeTimeCooking")
    private String recipeTimeCooking;
    @ColumnInfo(name="recipeImage")
    private String recipeImage;

    public RecipeEntity(String recipeName) {
        this.recipeName = recipeName;
    }

    @Ignore
    public RecipeEntity(int recipeId, String recipeName, String recipeDescription, String recipeTimePreparation, String recipeTimeCooking, String recipeImage){
        this.recipeId = recipeId;
        this.recipeName = recipeName;
        this.recipeDescription = recipeDescription;
        this.recipeTimePreparation = recipeTimePreparation;
        this.recipeTimeCooking = recipeTimeCooking;
        this.recipeImage = recipeImage;
    }

    public int getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getRecipeDescription() {
        return recipeDescription;
    }

    public void setRecipeDescription(String recipeDescription) {
        this.recipeDescription = recipeDescription;
    }

    public String getRecipeTimePreparation() {
        return recipeTimePreparation;
    }

    public void setRecipeTimePreparation(String recipeTimePreparation) {
        this.recipeTimePreparation = recipeTimePreparation;
    }

    public String getRecipeTimeCooking() {
        return recipeTimeCooking;
    }

    public void setRecipeTimeCooking(String recipeTimeCooking) {
        this.recipeTimeCooking = recipeTimeCooking;
    }

    public String getRecipeImage() {
        return recipeImage;
    }

    public void setRecipeImage(String recipeImage) {
        this.recipeImage = recipeImage;
    }
}
