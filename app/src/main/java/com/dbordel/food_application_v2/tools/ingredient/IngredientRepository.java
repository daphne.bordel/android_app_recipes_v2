package com.dbordel.food_application_v2.tools.ingredient;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.dbordel.food_application_v2.data.db.AppDatabase;
import com.dbordel.food_application_v2.data.db.IngredientDao;
import com.dbordel.food_application_v2.data.db.model.IngredientEntity;

import java.util.List;

public class IngredientRepository {
    private IngredientDao mIngredientDao;
    private LiveData<List<IngredientEntity>> mAllIngredients;

    public IngredientRepository(Application application){
        AppDatabase db = AppDatabase.getDatabaseInstance(application);
        mIngredientDao = db.getIngredientDao();
        mAllIngredients = mIngredientDao.getAllIngredients();
    }

    /*-------------------------------------- CRUD -----------------------------------------*/

    /* ---------------------- GET ONE INGREDIENT ------------------------------*/
    public IngredientEntity getIngredientById(int id){
        return mIngredientDao.getIngredientById(id);
    }

    public List<IngredientEntity> getIngredientByName(String ingredient){
        return mIngredientDao.getIngredientByName(ingredient);
    }

    /* ---------------------- GET ALL INGREDIENTS ------------------------------*/
    public LiveData<List<IngredientEntity>> getAllIngredients(){
        return mAllIngredients;
    }

    /* ---------------------- DELETE ALL INGREDIENTS ------------------------------*/
    public void delete(IngredientEntity ingredient){
        new IngredientRepository.deleteAsyncTask(mIngredientDao).execute(ingredient);
    }

    /* ---------------- DELETE INGREDIENT BY INGREDIENT ID -----------------------*/
    public static class deleteAsyncTask extends  AsyncTask<IngredientEntity, Void, Void>{
        private IngredientDao mAsyncTaskDao;

        public deleteAsyncTask(IngredientDao ingredientDao) {
            mAsyncTaskDao = ingredientDao;
        }

        @Override
        protected Void doInBackground(IngredientEntity... ingredientEntities) {
            mAsyncTaskDao.deleteIngredient(ingredientEntities[0].getIngredientId());
            return null;
        }
    }

    public void deleteAllIngredients(){
        new IngredientRepository.deleteAllAsyncTask(mIngredientDao).execute();
    }

    private static class deleteAllAsyncTask extends AsyncTask<IngredientEntity, Void, Void> {
        private IngredientDao mAsyncTaskDao;

        public deleteAllAsyncTask(IngredientDao ingredientDao) {
            mAsyncTaskDao = ingredientDao;
        }

        @Override
        protected Void doInBackground(IngredientEntity... ingredientEntities) {
            mAsyncTaskDao.deleteAllIngrdients();
            return null;
        }
    }

    /* -------------------------- CREATE INGREDIENT -------------------------------*/
    public void insert(IngredientEntity ingredient){
        new IngredientRepository.insertAsyncTask(mIngredientDao).execute(ingredient);
    }

    public static class insertAsyncTask extends AsyncTask<IngredientEntity, Void, Void>{
        private IngredientDao mAsyncTaskDao;
        insertAsyncTask(IngredientDao dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected  Void doInBackground(final IngredientEntity... params){
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
    /* ------------------- UPDATE INGREDIENT -----------------------------------*/
    public void update(IngredientEntity ingredient){
        new IngredientRepository.updateAsyncTask(mIngredientDao).execute(ingredient);
    }

    public static class updateAsyncTask extends AsyncTask<IngredientEntity, Void, Void>{
        private IngredientDao mAsyncTaskDao;

        updateAsyncTask(IngredientDao ingredientDao) {
            mAsyncTaskDao = ingredientDao;
        }

        @Override
        protected Void doInBackground(IngredientEntity... ingredientEntities) {
            mAsyncTaskDao.update(ingredientEntities[0]);
            return null;
        }
    }
}
