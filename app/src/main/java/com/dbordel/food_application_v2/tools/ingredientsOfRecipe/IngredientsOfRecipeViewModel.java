package com.dbordel.food_application_v2.tools.ingredientsOfRecipe;

import android.annotation.SuppressLint;
import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.room.Room;

import com.dbordel.food_application_v2.data.db.model.RecipeEntity;
import com.dbordel.food_application_v2.data.db.model.RoomRecipeIngredient;

import java.util.List;

public class IngredientsOfRecipeViewModel extends AndroidViewModel {

    private IngredientsOfRecipeRepository mIngredientsOfRecipeRepository;
    private final LiveData<List<RoomRecipeIngredient>> mAllRecipesIngredients;

    public IngredientsOfRecipeViewModel(@NonNull Application application) {
        super(application);
        mIngredientsOfRecipeRepository = new IngredientsOfRecipeRepository(application);
        this.mAllRecipesIngredients = mIngredientsOfRecipeRepository.getAllRecipesIngredient();
    }

    // Getter
    public LiveData<List<RoomRecipeIngredient>> getAllRecipesIngredients() {
        return mAllRecipesIngredients;
    }

    /* ------------ DEFINE HOW INSERT A ROOM-RECIPE-INGREDIENT IN THE TABLE ----------*/
    public void insert(RoomRecipeIngredient recipeIngredient){
        mIngredientsOfRecipeRepository.insert(recipeIngredient);
    }

    /* ------------ DEFINE HOW GET ALL ROOM-RECIPE-INGREDIENT FOR A RECIPE ----------*/
    public LiveData<List<RoomRecipeIngredient>> getAllRoomRecipeIngredientByRecipeId(int recipeId){
        return mIngredientsOfRecipeRepository.getAllRoomRecipeIngredientByRecipeId(recipeId);
    }

    /* ------------------------- DELETE ROOM RECIPE INGREDIENTS BY ID ---------------*/
    public Void removeRecipeIngredient(RoomRecipeIngredient roomRecipeIngredient){
        return mIngredientsOfRecipeRepository.delete(roomRecipeIngredient);
    }

    public Void removeAllIngredientsOfRecipe(int recipeId){
        return mIngredientsOfRecipeRepository.deleteAllIngredientsByRecipeId(recipeId);
    }
}
