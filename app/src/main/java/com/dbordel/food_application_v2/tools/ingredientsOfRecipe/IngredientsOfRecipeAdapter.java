package com.dbordel.food_application_v2.tools.ingredientsOfRecipe;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.food_application_v2.R;
import com.dbordel.food_application_v2.data.db.model.RoomRecipeIngredient;
import com.dbordel.food_application_v2.tools.ingredient.IngredientViewModel;
import com.dbordel.food_application_v2.ui.activities.MyRecipeActivities.MyRecipeDetailActivity;


import java.util.List;

public class IngredientsOfRecipeAdapter extends RecyclerView.Adapter<IngredientsOfRecipeAdapter.IngredientsOfRecipeViewHolder> {
    private final LayoutInflater mInflater;
    private List<RoomRecipeIngredient> mRoomRecipeIngredients;
    private IngredientsOfRecipeViewModel mIngredientsOfRecipeViewModel;
    private IngredientViewModel mIngredientViewModel;
    private String mIngredientName;
    private List<RoomRecipeIngredient> mRoomRecipeIngredientTemp;
    Context mContext;
    @LayoutRes int res;

    public IngredientsOfRecipeAdapter(Context context, Application application, List<RoomRecipeIngredient> roomRecipeIngredients, @LayoutRes int res){
        this.mRoomRecipeIngredients = roomRecipeIngredients;
        this.mRoomRecipeIngredientTemp = roomRecipeIngredients;
        this.mInflater = LayoutInflater.from(context);
        mIngredientViewModel = new IngredientViewModel(application);
        mIngredientsOfRecipeViewModel = new IngredientsOfRecipeViewModel(application);
        mContext = context;
        this.res = res;
    }

    public static int getAdapterPosition() {
        return getAdapterPosition();
    }

    @NonNull
    @Override
    public IngredientsOfRecipeAdapter.IngredientsOfRecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Define view uses by the recyclerview
        View itemView = mInflater.inflate(res, parent, false);
        return new IngredientsOfRecipeAdapter.IngredientsOfRecipeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull IngredientsOfRecipeAdapter.IngredientsOfRecipeViewHolder holder, int position) {
        if (mRoomRecipeIngredients != null){
            // get the current roomRecipeIngredient
            RoomRecipeIngredient current = mRoomRecipeIngredients.get(position);
            holder.display(current);
        }
    }

    void setRecipes(List<RoomRecipeIngredient> roomRecipeIngredients){
        mRoomRecipeIngredients = roomRecipeIngredients;
    }

    @Override
    public int getItemCount() {
        if (mRoomRecipeIngredients != null){
            return mRoomRecipeIngredients.size();
        } else return 0;
    }

    public class IngredientsOfRecipeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView quantityItemView;
        private TextView measureItemView;
        private TextView ingredientItemView;
        private ImageView btnDeleteIngredientsOfRecipe;
        private Button btnCancelChangementRecipe;

        @SuppressLint("LongLogTag")
        public IngredientsOfRecipeViewHolder(View itemView){
            super(itemView);
            quantityItemView = (TextView) itemView.findViewById(R.id.quantity_ingredient);
            measureItemView = (TextView) itemView.findViewById(R.id.measure_ingredient);
            ingredientItemView = (TextView) itemView.findViewById(R.id.ingredient);

            btnDeleteIngredientsOfRecipe = itemView.findViewById(R.id.delete_ingredient_of_recipe);
            Log.d("button delete", String.valueOf(btnDeleteIngredientsOfRecipe));
            if (btnDeleteIngredientsOfRecipe != null){
                btnDeleteIngredientsOfRecipe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("v in setOnClickListener", String.valueOf(v));
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            //in adapter the room recipe ingredients will be deleted.
                            // if user cancel the operation we must store temporarly the item deleted.
                            mIngredientsOfRecipeViewModel.removeRecipeIngredient(mRoomRecipeIngredients.get(position));
                        }
                    }
                });
            }

        }

        //display data for the item view corresponded
        public void display(RoomRecipeIngredient recipeIngredient) {
            quantityItemView.setText(recipeIngredient.getQuantity());
            mIngredientName = mIngredientViewModel.getIngredientById(recipeIngredient.getIngredientId()).getIngredientName();
            ingredientItemView.setText(mIngredientName);
            measureItemView.setText(recipeIngredient.getMeasureUnit());
        }



        @Override
        public void onClick(View v) {

        }
    }
}
