package com.dbordel.food_application_v2.tools.myRecipe;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.dbordel.food_application_v2.data.db.AppDatabase;
import com.dbordel.food_application_v2.data.db.RecipeDao;
import com.dbordel.food_application_v2.data.db.model.RecipeEntity;

import java.util.List;

public class MyRecipeRepository {
    private RecipeDao mRecipeDao;
    private LiveData<List<RecipeEntity>> mAllRecipes;

    public MyRecipeRepository(Application application){
        AppDatabase db = AppDatabase.getDatabaseInstance(application);
        mRecipeDao = db.getRecipeDao();
        mAllRecipes = mRecipeDao.getAllRecipes();
    }

    public int countAllRecipes(){
        return mRecipeDao.countAllRecipes();
    }


    /* ---------------------- GET ONE RECIPE -----------------------*/
    public RecipeEntity getRecipeById(int id){
        return mRecipeDao.getRecipeById(id);
    }

    public RecipeEntity getRecipeByName(String recipeName) {
        return mRecipeDao.getRecipeByName(recipeName);
    }

    /*------------------------- GET ALL RECIPES --------------------------*/
    public LiveData<List<RecipeEntity>> getAllRecipes(){
        return mAllRecipes;
    }

    /* -------------------- UPDATE ONE RECIPE ----------------------------*/

    public int updateRecipe(RecipeEntity recipe){
        return mRecipeDao.updateRecipe(recipe);
    }

    /* ---------------------- CREATE ONE RECIPE ----------------------------*/
    public void insert(RecipeEntity recipe){
        new MyRecipeRepository.insertAsyncTask(mRecipeDao).execute(recipe);
    }

    private static class insertAsyncTask extends AsyncTask<RecipeEntity, Void, Void>{
        private RecipeDao mAsyncTaskDao;

        insertAsyncTask(RecipeDao dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final RecipeEntity... params){
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    /*----------------------- REMOVE ONE RECIPE -------------------------*/
    public void delete(RecipeEntity recipe){
        new MyRecipeRepository.deleteAsyncTask(mRecipeDao).execute(recipe);
    }

    public class deleteAsyncTask extends AsyncTask<RecipeEntity, Void, Void>{
        private RecipeDao mAsyncTaskDao;

        public deleteAsyncTask(RecipeDao recipeDao) {
            mAsyncTaskDao = recipeDao;
        }

        @Override
        protected Void doInBackground(RecipeEntity... recipeEntities) {
            mAsyncTaskDao.delete(recipeEntities[0]);
            return null;
        }
    }
}