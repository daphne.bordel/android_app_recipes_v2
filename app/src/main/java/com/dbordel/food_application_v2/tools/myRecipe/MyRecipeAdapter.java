package com.dbordel.food_application_v2.tools.myRecipe;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.food_application_v2.R;
import com.dbordel.food_application_v2.data.db.model.RecipeEntity;

import java.util.List;

public class MyRecipeAdapter extends RecyclerView.Adapter<MyRecipeAdapter.RecipeViewHolder> {
    private final LayoutInflater mInflater;
    private List<RecipeEntity> mRecipes;

    public MyRecipeAdapter(Context context, List<RecipeEntity> recipeEntities){
        mInflater = LayoutInflater.from(context);
        mRecipes = recipeEntities;
    }

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Define view uses by the recyclerview
        View itemView = mInflater.inflate(R.layout.card_my_recipe_model, parent, false);
        return new RecipeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyRecipeAdapter.RecipeViewHolder holder, int position) {
        if (mRecipes != null){
            // get the current recipe
            RecipeEntity current = mRecipes.get(position);
            // display the current recipe
            holder.display(current);
        }
    }

    void setRecipes(List<RecipeEntity> recipes){
        mRecipes = recipes;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mRecipes != null){
            return mRecipes.size();
        } else return 0;
    }

    public class RecipeViewHolder extends RecyclerView.ViewHolder{
        private final TextView recipeItemViewName;

        private RecipeViewHolder(View itemView){
            super(itemView);
            recipeItemViewName = itemView.findViewById(R.id.my_recipe_name);
        }

        // function to display all elements of recipe in the view
        public void display(RecipeEntity recipe) {
            recipeItemViewName.setText(recipe.getRecipeName());

        }
    }
}
