package com.dbordel.food_application_v2.tools.ingredientsOfRecipe;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

import com.dbordel.food_application_v2.data.db.AppDatabase;
import com.dbordel.food_application_v2.data.db.RecipeIngredientsDao;
import com.dbordel.food_application_v2.data.db.model.RoomRecipeIngredient;

import java.util.List;

public class IngredientsOfRecipeRepository {
    private RecipeIngredientsDao mRecipeIngredientsDao;
    private LiveData<List<RoomRecipeIngredient>> mRecipesIngredientsLD;

    public IngredientsOfRecipeRepository(Application application){
        AppDatabase db = AppDatabase.getDatabaseInstance(application);
        mRecipeIngredientsDao = db.getRecipeIngredientsDao();
        mRecipesIngredientsLD = mRecipeIngredientsDao.getRecipesIngredients();
    }

    /* --------------------- GET ALL ROOM-RECIPE-INGREDIENT -------------------------*/
    public LiveData<List<RoomRecipeIngredient>> getAllRecipesIngredient(){
        return mRecipesIngredientsLD;
    }

    /* ------------------ GET ALL ROOM-RECIPES-INGREDIENTS FOR ONE RECIPE --------------*/
    public LiveData<List<RoomRecipeIngredient>> getAllRoomRecipeIngredientByRecipeId(int id){
        return mRecipeIngredientsDao.getAllRoomRecipeIngredientByRecipeId(id);
    }

    public List<RoomRecipeIngredient> getIngredientsByRecipeId(int id){
        return mRecipeIngredientsDao.getIngredientsByRecipeId(id);
    }

    /* ----------------------- ADD A ROOM-RECIPE INGREDIENT ----------------------------*/

    public void insert(RoomRecipeIngredient recipeIngredient){
        new IngredientsOfRecipeRepository.insertAsyncTask(mRecipeIngredientsDao).execute(recipeIngredient);
    }

    public String insertAllIngredientsByRecipeId(List<RoomRecipeIngredient> ingredientsOfRecipe){
        for(int i=0; i<ingredientsOfRecipe.size();i++){
            new IngredientsOfRecipeRepository.insertAsyncTask(mRecipeIngredientsDao).execute(ingredientsOfRecipe.get(i));
        }
        return "OK";
    }

    private static class insertAsyncTask extends AsyncTask<RoomRecipeIngredient, Void, Void> {
        private RecipeIngredientsDao mAsyncTaskDao;
        insertAsyncTask(RecipeIngredientsDao dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected  Void doInBackground(final RoomRecipeIngredient... params){
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    /*---------------- DELETE A ROOM-RECIPE-INGREDIENTS ----------------------*/
    public Void delete(RoomRecipeIngredient recipeIngredient){
        new IngredientsOfRecipeRepository.deleteAsyncTask(mRecipeIngredientsDao).execute(recipeIngredient);
        return null;
    }

    public Void deleteAllIngredientsByRecipeId(int recipeId){
        //get all ingredients of the current recipe
        List<RoomRecipeIngredient> allIngredientsOfRecipeId = mRecipeIngredientsDao.getIngredientsByRecipeId(recipeId);
        for (int i=0; i<allIngredientsOfRecipeId.size();i++){
            new IngredientsOfRecipeRepository.deleteAsyncTask(mRecipeIngredientsDao).execute(allIngredientsOfRecipeId.get(i));
        }
        return null;
    }

    private static class deleteAsyncTask extends AsyncTask<RoomRecipeIngredient, Void, Void> {
        private RecipeIngredientsDao mAsyncTaskDao;

        public deleteAsyncTask(RecipeIngredientsDao recipeIngredientsDao) {
            mAsyncTaskDao = recipeIngredientsDao;
        }

        @Override
        protected Void doInBackground(RoomRecipeIngredient... roomRecipeIngredients) {
            mAsyncTaskDao.delete(roomRecipeIngredients[0].getId());
            return null;
        }
    }


}
