package com.dbordel.food_application_v2.tools.ingredient;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.food_application_v2.R;
import com.dbordel.food_application_v2.data.db.model.IngredientEntity;
import com.dbordel.food_application_v2.ui.activities.IngredientActivites.IngredientMainActivity;
import com.dbordel.food_application_v2.ui.activities.IngredientActivites.IngredientsCreateActivity;

import java.util.List;

public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.IngredientViewHolder> {
    private final LayoutInflater mInflater;
    private List<IngredientEntity> mIngredients;
    Application application;
    Context mContext;
    int currentRecipeId;

    public IngredientAdapter(Context context, List<IngredientEntity> ingredients, Application application, int currentRecipeId){
        this.mIngredients = ingredients;
        this.mInflater = LayoutInflater.from(context);
        this.application = application;
        this.mContext = context;
        this.currentRecipeId = currentRecipeId;
    }

    @NonNull
    @Override
    public IngredientAdapter.IngredientViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Define view uses by the recyclerview
        View itemView = mInflater.inflate(R.layout.manage_ingredients_list, parent, false);
        return new IngredientAdapter.IngredientViewHolder(itemView);
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onBindViewHolder(@NonNull IngredientAdapter.IngredientViewHolder holder, int position) {
        if (mIngredients != null){
            // get the current ingredient
            IngredientEntity current = mIngredients.get(position);
            //display the current ingredient
            holder.display(current);
        }
    }

    void setRecipes(List<IngredientEntity> ingredients){
        mIngredients = ingredients;
    }

    @Override
    public int getItemCount() {
        if (mIngredients != null){
            return mIngredients.size();
        } else return 0;
    }

    public class IngredientViewHolder extends RecyclerView.ViewHolder{
        private TextView ingredientItemViewName;
        IngredientViewModel mIngredientViewModel = new IngredientViewModel(application);


        private IngredientViewHolder(View itemView){
            super(itemView);
            ingredientItemViewName = (TextView) itemView.findViewById(R.id.item_setText);

            //edit an ingredient
            ImageView btnEditIngredient = itemView.findViewById(R.id.edit_ingredient);
            if (btnEditIngredient != null){
                btnEditIngredient.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("v in setOnClickListener", String.valueOf(v));
                        int position = getAdapterPosition();
                        Log.d("position", String.valueOf(position));
                        if(position != RecyclerView.NO_POSITION){
                            // start activity for edit ingredient
                            Intent intent = new Intent(mContext, IngredientsCreateActivity.class);
                            intent.putExtra("ingredientId", mIngredients.get(position).getIngredientId());
                            Log.d("adapter for ingredient", String.valueOf(mIngredients.get(position).getIngredientId()));
                            intent.putExtra("recipeId",currentRecipeId);
                            mContext.startActivity(intent);
                        }
                    }
                });
            }
            //edit an ingredient
            ImageView btnDeleteIngredient = itemView.findViewById(R.id.delete_ingredient);
            if (btnDeleteIngredient != null){
                btnDeleteIngredient.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("v in setOnClickListener", String.valueOf(v));
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            //in adapter the room recipe ingredients will be deleted.
                            // if user cancel the operation we must store temporarly the item deleted.
                            mIngredientViewModel.deleteIngredient(mIngredients.get(position));
                        }
                    }
                });
            }
        }

        //display data for the item view corresponded
        public void display(IngredientEntity ingredient) {
            ingredientItemViewName.setText(ingredient.getIngredientName());
        }
    }
}
