package com.dbordel.food_application_v2.tools.myRecipe;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.dbordel.food_application_v2.data.db.model.RecipeEntity;
import com.dbordel.food_application_v2.tools.myRecipe.MyRecipeRepository;

import java.util.List;

public class RecipeViewModel extends AndroidViewModel {
    private MyRecipeRepository mMyRecipeRepository;
    private LiveData<List<RecipeEntity>> mAllRecipes;

    public RecipeViewModel(Application application){
        super(application);
        mMyRecipeRepository = new MyRecipeRepository(application);
        mAllRecipes = mMyRecipeRepository.getAllRecipes();
    }

    /* --------------------- define how selected all recipes -----------------------*/
    public LiveData<List<RecipeEntity>> getAllRecipes(){
        return mAllRecipes;
    }

    /* --------------------- define how selected one ingredient -----------------------*/
    public RecipeEntity getRecipeById(int id){
        return mMyRecipeRepository.getRecipeById(id);
    }

    public RecipeEntity getRecipeByName(String recipeName){
       return mMyRecipeRepository.getRecipeByName(recipeName);
    }

    /* --------------------- define how updated one ingredient -----------------------*/
    public int updateRecipe(RecipeEntity recipe){
        return mMyRecipeRepository.updateRecipe(recipe);
    }

    /* --------------------- define how created one ingredient -----------------------*/
    public void insert(RecipeEntity recipe){
        mMyRecipeRepository.insert(recipe);
    }

    /* ------------------ define how deleted recipe -------------------------*/
    public void removeRecipe(RecipeEntity recipe){
        mMyRecipeRepository.delete(recipe);
    }
}
