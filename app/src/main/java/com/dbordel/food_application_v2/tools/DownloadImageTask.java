package com.dbordel.food_application_v2.tools;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;

    public DownloadImageTask(ImageView imageView) {
        this.bmImage = imageView;
    }

    public void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
    }

    @Override
    protected Bitmap doInBackground(String... urls) {
        String urlDisplay = urls[0];
        Bitmap bitmap = null;
        try {
            InputStream stream = new URL(urlDisplay).openStream();
            bitmap = BitmapFactory.decodeStream(stream);
        } catch (MalformedURLException e) {
            Log.e("Error",e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}