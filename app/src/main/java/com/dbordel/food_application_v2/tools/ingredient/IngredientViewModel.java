package com.dbordel.food_application_v2.tools.ingredient;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.dbordel.food_application_v2.data.db.model.IngredientEntity;

import java.util.List;

public class IngredientViewModel extends AndroidViewModel {
    private IngredientRepository mIngredientRepository;
    private LiveData<List<IngredientEntity>> mAllIngredients;

    public IngredientViewModel(Application application){
        super(application);
        mIngredientRepository = new IngredientRepository(application);
        mAllIngredients = mIngredientRepository.getAllIngredients();
    }

    /* --------------------- define how selected one ingredient -----------------------*/
    public IngredientEntity getIngredientById(int id){
        return mIngredientRepository.getIngredientById(id);
    }

    public List<IngredientEntity> getIngredientByName(String ingredientName){
        return mIngredientRepository.getIngredientByName(ingredientName);
    }

    /* --------------------- define how selected all ingredients -----------------------*/
    public LiveData<List<IngredientEntity>> getAllIngredients(){
        return mAllIngredients;
    }

    /* --------------------- define how delete all ingredients -----------------------*/
    public LiveData<List<IngredientEntity>> deleteAllIngredients(){
        return mAllIngredients;
    }

    public Void deleteIngredient(IngredientEntity ingredient){
        mIngredientRepository.delete(ingredient);
        return null;
    }
    /* --------------------- define how create one ingredient -----------------------*/
    public void insert(IngredientEntity ingredient){
        mIngredientRepository.insert(ingredient);
    }

    /*--------------------- define how to update one ingredient --------------------*/
    public void updateIngredient(IngredientEntity ingredient){
        mIngredientRepository.update(ingredient);
    }
}
