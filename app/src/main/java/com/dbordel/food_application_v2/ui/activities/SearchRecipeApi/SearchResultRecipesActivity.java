package com.dbordel.food_application_v2.ui.activities.SearchRecipeApi;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.food_application_v2.R;
import com.dbordel.food_application_v2.data.network.model.ApiRecipe;
import com.dbordel.food_application_v2.tools.apiRecipe.ApiRecipeAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchResultRecipesActivity extends AppCompatActivity {
    private ApiRecipeAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private JSONArray mRecipeList;
    private List<ApiRecipe> mRecipes = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_result_recipe_activity);
        // intent
        Intent intent = getIntent();
        String jsonString = intent.getStringExtra("data");
        JSONObject jsonObject = null;
        try {
            jsonObject= new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            mRecipeList = jsonObject.getJSONArray("recipes");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i=0; i< mRecipeList.length(); i++){
            try {
                JSONObject recipe = (JSONObject) mRecipeList.get(i);
                ApiRecipe myRecipe = new ApiRecipe(
                        recipe.getString("title"),
                        recipe.getString("image_url"),
                        recipe.getString("publisher"),
                        recipe.getString("source_url"),
                        recipe.getInt("social_rank")
                );
                mRecipes.add(myRecipe);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //recycler view
        if (mRecipes.size()>0){
            mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_items_search);
            // Create adapter passing in the sample recipe data
            mAdapter = new ApiRecipeAdapter(mRecipes,this);
            // Attach the adapter to the recyclerview to populate items
            mRecyclerView.setAdapter(mAdapter);
            // Set layout manager to position the items
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
    }
}
