package com.dbordel.food_application_v2.ui.activities.MyRecipeActivities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.food_application_v2.R;
import com.dbordel.food_application_v2.data.db.model.EnumMeasureUnit;
import com.dbordel.food_application_v2.data.db.model.IngredientEntity;
import com.dbordel.food_application_v2.data.db.model.RecipeEntity;
import com.dbordel.food_application_v2.data.db.model.RoomRecipeIngredient;
import com.dbordel.food_application_v2.tools.ingredient.IngredientViewModel;
import com.dbordel.food_application_v2.tools.ingredientsOfRecipe.IngredientsOfRecipeAdapter;
import com.dbordel.food_application_v2.tools.ingredientsOfRecipe.IngredientsOfRecipeViewModel;
import com.dbordel.food_application_v2.tools.myRecipe.RecipeViewModel;
import com.dbordel.food_application_v2.ui.activities.IngredientActivites.IngredientMainActivity;

import java.util.ArrayList;
import java.util.List;

public class MyRecipeIngredientsUpdate extends AppCompatActivity {
    IngredientsOfRecipeAdapter mIngredientsOfRecipeAdapter;

    LiveData<List<RoomRecipeIngredient>> mRoomRecipeIngredientListLD;
    List<RoomRecipeIngredient> mRoomRecipeIngredientsList;
    RecipeEntity mCurrentRecipe;

    List<IngredientEntity> currentIngredients;

    IngredientsOfRecipeViewModel mIngredientsOfRecipeViewModel;
    RecipeViewModel mRecipeViewModel;
    IngredientViewModel mIngredientViewModel;

    String recipeName;
    ArrayList<String> mIngredientListName;

    // spinner in the view
    Spinner mSpinnerMeasure;
    Spinner mSpinnerIngredients;
    // quantity edit text
    EditText quantityEditText;

    Button btnReturnRecipe;

    String quantity;
    String ingredientName;
    String measure;

    RecyclerView listIngredientRv;


    @SuppressLint("LongLogTag")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_update_ingredients_of_recipe);

        //construct viewModel
        mIngredientViewModel = new IngredientViewModel(getApplication());
        mRecipeViewModel = new RecipeViewModel(getApplication());
        mIngredientsOfRecipeViewModel = new IngredientsOfRecipeViewModel(getApplication());

        //get the fields edit text for the quantity of ingredient
        quantityEditText = findViewById(R.id.edt_measure_quantity);

        //get the current recipe id
        Intent intent = getIntent();
        int recipeId = intent.getIntExtra("recipeId", 0);
        Log.d("recipeId here", String.valueOf(recipeId));
        // get name of recipe
        mCurrentRecipe = mRecipeViewModel.getRecipeById(recipeId);
        Log.d("current recipe name",mCurrentRecipe.getRecipeName());
        //get all ingredients added for the current recipe
        mRoomRecipeIngredientListLD = mIngredientsOfRecipeViewModel.getAllRoomRecipeIngredientByRecipeId(recipeId);
        mRoomRecipeIngredientListLD.observe(this, new Observer<List<RoomRecipeIngredient>>() {
            @Override
            public void onChanged(List<RoomRecipeIngredient> roomRecipeIngredients) {
                //recycler view
                listIngredientRv = (RecyclerView) findViewById(R.id.recyclerview_ingredients_list_manage);
                // Create adapter passing in the sample recipe data
                if (roomRecipeIngredients.size()>0){
                    mIngredientsOfRecipeAdapter = new IngredientsOfRecipeAdapter(MyRecipeIngredientsUpdate.this, getApplication(), roomRecipeIngredients, R.layout.ingredient_list_for_recipe_with_manage);
                    // Attach the adapter to the recyclerview to populate items
                    listIngredientRv.setAdapter(mIngredientsOfRecipeAdapter);
                    // Set layout manager to position the items
                    listIngredientRv.setLayoutManager(new LinearLayoutManager(MyRecipeIngredientsUpdate.this));
                }
            }
        });

        //show list of unity measure to the spinner
        mSpinnerMeasure = (Spinner) findViewById(R.id.spinner_measure);
        mSpinnerMeasure.setAdapter(new ArrayAdapter<EnumMeasureUnit>(this,R.layout.simple_list_item, R.id.tv_item_measure, EnumMeasureUnit.values()));

        //manage the selection in spinner "measure"
        mSpinnerMeasure.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //store measure selected
                measure = mSpinnerMeasure.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //error
            }
        });

        //Spinner ingredients
        mSpinnerIngredients = (Spinner) findViewById(R.id.spinner_ingredient);
        //manage the selection in spinner "ingredients"
        mSpinnerIngredients.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //store the ingredient name selected
                ingredientName = mSpinnerIngredients.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //error
            }
        });

        //show list of ingredients to the spinner
        mIngredientViewModel.getAllIngredients().observe(this, new Observer<List<IngredientEntity>>() {
            @Override
            public void onChanged(List<IngredientEntity> ingredientEntities) {
                mIngredientListName = new ArrayList<String>();
                for(int i=0; i<ingredientEntities.size(); i++){
                    // get all name of ingredients table for showing them in spinner ingredients
                    mIngredientListName.add(ingredientEntities.get(i).getIngredientName());
                    mSpinnerIngredients.setAdapter(new ArrayAdapter<String>(MyRecipeIngredientsUpdate.this, R.layout.simple_list_item, mIngredientListName));
                }
            }
        });

        //Show page to manage ingredients (creation new ingredients / update / delete...)
        Button buttonManageIngredients = findViewById(R.id.btn_manage_ingredients);
        buttonManageIngredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIngredientsListActivity = new Intent(getApplicationContext(), IngredientMainActivity.class);
                myIngredientsListActivity.putExtra("recipeId",recipeId);
                startActivity(myIngredientsListActivity);
            }
        });

        //Add ingredient with measure and quantity for the current recipe
        Button buttonAddIngredient = findViewById(R.id.btn_add_ingredient);
        buttonAddIngredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentIngredients = mIngredientViewModel.getIngredientByName(ingredientName);
                quantity = quantityEditText.getText().toString();
                // create a new RoomRecipeIngredient
                RoomRecipeIngredient mRoomRecipeIngredient = new RoomRecipeIngredient(
                        currentIngredients.get(0).getIngredientId(),
                        mCurrentRecipe.getRecipeId(),
                        quantity,
                        measure
                );
                mIngredientsOfRecipeViewModel.insert(mRoomRecipeIngredient);
                // clear the field
                quantityEditText.getText().clear();
            }
        });

        btnReturnRecipe = findViewById(R.id.btn_return_recipe_update);
        //return to the recipe update
        btnReturnRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
