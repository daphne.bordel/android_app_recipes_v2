package com.dbordel.food_application_v2.ui.activities.MyRecipeActivities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.food_application_v2.R;
import com.dbordel.food_application_v2.data.db.model.RecipeEntity;
import com.dbordel.food_application_v2.data.db.model.RoomRecipeIngredient;
import com.dbordel.food_application_v2.tools.DownloadImageTask;
import com.dbordel.food_application_v2.tools.ingredientsOfRecipe.IngredientsOfRecipeAdapter;
import com.dbordel.food_application_v2.tools.ingredientsOfRecipe.IngredientsOfRecipeViewModel;
import com.dbordel.food_application_v2.tools.myRecipe.RecipeViewModel;

import org.w3c.dom.Text;

import java.util.List;

public class MyRecipeDetailActivity extends AppCompatActivity {
    int mRecipeId;
    RecipeViewModel mRecipeViewModel;
    RecipeEntity mRecipeEntity = null;
    TextView tvRecipeTitle;
    TextView tvRecipeDescription;
    TextView tvPreparationTime;
    TextView tvCookingTime;
    ImageView tvRecipeImage;
    RecyclerView listIngredientRv;
    IngredientsOfRecipeAdapter mIngredientsOfRecipeAdapter;
    IngredientsOfRecipeViewModel mIngredientsOfRecipeViewModel;

    // for get all ingredients for one recipe
    LiveData<List<RoomRecipeIngredient>> mGetListOfIngredientsLD;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_recipe_activity);

        //item view
        tvRecipeTitle = (TextView) findViewById(R.id.recipe_detail_title);
        tvRecipeDescription = (TextView) findViewById(R.id.recipe_detail_description);
        tvPreparationTime = (TextView) findViewById(R.id.recipe_detail_preparation_time);
        tvCookingTime = (TextView) findViewById(R.id.recipe_detail_cooking_time);
        tvRecipeImage = (ImageView) findViewById(R.id.recipe_detail_image);
        listIngredientRv = (RecyclerView) findViewById(R.id.rv_list_ingredients_currentRecipe);

        //init view model
        mRecipeViewModel = new RecipeViewModel(getApplication());
        mIngredientsOfRecipeViewModel = new IngredientsOfRecipeViewModel(getApplication());

        //get information of recipe clicked
        Intent intent = getIntent();
        mRecipeId = intent.getIntExtra("recipeId",0);
        Log.d("recipe ID", String.valueOf(mRecipeId));
        try {
            mRecipeEntity = mRecipeViewModel.getRecipeById(mRecipeId);
            Log.d("RecipeEntity", String.valueOf(mRecipeEntity));
            tvRecipeTitle.setText(mRecipeEntity.getRecipeName());
            tvRecipeDescription.setText(mRecipeEntity.getRecipeDescription());
            tvPreparationTime.setText(mRecipeEntity.getRecipeTimePreparation());
            tvCookingTime.setText(mRecipeEntity.getRecipeTimeCooking());
            //DownloadImageTask imageTask = new DownloadImageTask(tvRecipeImage);

            //get all ingredients for the current recipe
            mGetListOfIngredientsLD = mIngredientsOfRecipeViewModel.getAllRoomRecipeIngredientByRecipeId(mRecipeId);
            Log.d("mGetListOfIngredientsLD", String.valueOf(mGetListOfIngredientsLD));
            // transform live data list of ingredients for the current recipe to RoomRecipeIngredient entity
            mGetListOfIngredientsLD.observe(this, new Observer<List<RoomRecipeIngredient>>(){
                @Override
                public void onChanged(List<RoomRecipeIngredient> roomRecipeIngredients) {
                    Log.d("size of", String.valueOf(roomRecipeIngredients.size()));
                    // Create adapter passing in the sample recipe data
                    mIngredientsOfRecipeAdapter = new IngredientsOfRecipeAdapter(MyRecipeDetailActivity.this, getApplication(), roomRecipeIngredients,R.layout.ingredient_list_for_recipe);
                    // Attach the adapter to the recyclerview to populate items
                    listIngredientRv.setAdapter(mIngredientsOfRecipeAdapter);
                    // Set layout manager to position the items
                    listIngredientRv.setLayoutManager(new LinearLayoutManager(MyRecipeDetailActivity.this));
                }
            });
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        // When click on the button "Mes recettes"
        Button buttonManageIngredients = findViewById(R.id.btn_my_recipes_home);
        buttonManageIngredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myRecipesListIntent = new Intent(MyRecipeDetailActivity.this, MyRecipeMainActivity.class);
                startActivity(myRecipesListIntent);
            }
        });

        // When click on the button "Editer"
        ImageButton buttonEditRecipe = findViewById(R.id.btn_recipe_detail_edit);
        buttonEditRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mUpdateRecipeIntent = new Intent(MyRecipeDetailActivity.this, MyRecipeUpdateActivity.class);
                Log.d("mRecipeId", String.valueOf(mRecipeId));
                mUpdateRecipeIntent.putExtra("recipeName",mRecipeEntity.getRecipeName());
                startActivity(mUpdateRecipeIntent);
            }
        });

        // When click on the button "Supprimer"
        ImageButton buttonDeleteRecipe = findViewById(R.id.btn_recipe_detail_delete);
        buttonDeleteRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("remove is clicked", String.valueOf(mRecipeEntity.getRecipeId()));
                mIngredientsOfRecipeViewModel.removeAllIngredientsOfRecipe(mRecipeId);
                mRecipeViewModel.removeRecipe(mRecipeEntity);
                Intent intent = new Intent(MyRecipeDetailActivity.this,MyRecipeMainActivity.class);
                startActivity(intent);

            }
        });


    }


}
