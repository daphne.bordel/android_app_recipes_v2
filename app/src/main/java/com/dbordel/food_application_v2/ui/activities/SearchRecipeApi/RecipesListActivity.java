package com.dbordel.food_application_v2.ui.activities.SearchRecipeApi;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.food_application_v2.R;
import com.dbordel.food_application_v2.data.network.HttpGetRequest;
import com.dbordel.food_application_v2.data.network.model.ApiRecipe;
import com.dbordel.food_application_v2.tools.apiRecipe.ApiRecipeAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class RecipesListActivity  extends AppCompatActivity {
    JSONArray mRecipesList;
    List<ApiRecipe> mRecipes = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private ApiRecipeAdapter mAdapter;
    private String query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_result_recipe_activity);

        Intent intent = getIntent();
        // get the query enters on the search view
        query = intent.getStringExtra("query");
        //show all recipes with chicken
        String myUrl = createURL(query);
        String result;
        HttpGetRequest myRequest = new HttpGetRequest();
        JSONObject obj = null;

        try {
            GsonBuilder gsonb = new GsonBuilder();
            Gson gson = gsonb.create();
            // execute request to fetch api url
            result = myRequest.execute(myUrl).get();
            obj = new JSONObject(result);
            // get the result of request fetching
            mRecipesList = obj.getJSONArray("recipes");
            for (int i=0; i< mRecipesList.length(); i++){
                try {
                    JSONObject recipe = (JSONObject) mRecipesList.get(i);
                    // create new ApiRecipe
                    ApiRecipe myRecipe = new ApiRecipe(
                            recipe.getString("title"),
                            recipe.getString("image_url"),
                            recipe.getString("publisher"),
                            recipe.getString("source_url"),
                            recipe.getInt("social_rank")
                    );
                    //store recipes finding by the fetching API to display them
                    mRecipes.add(myRecipe);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //recycler view
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_items_search);
        // Create adapter passing in the sample recipe data
        mAdapter = new ApiRecipeAdapter(mRecipes,this);
        // Attach the adapter to the recyclerview to populate items
        mRecyclerView.setAdapter(mAdapter);
        // Set layout manager to position the items
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    private String createURL(String query) {
        String baseUrl = "https://recipesapi.herokuapp.com/api/search?q=";
        try {
            String urlString = baseUrl + URLEncoder.encode(query, "UTF-8");
            return urlString;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}