package com.dbordel.food_application_v2.ui.activities.MyRecipeActivities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.dbordel.food_application_v2.R;
import com.dbordel.food_application_v2.data.db.model.RecipeEntity;
import com.dbordel.food_application_v2.tools.myRecipe.RecipeViewModel;

public class MyRecipeCreateActivity  extends AppCompatActivity {
    RecipeViewModel mRecipeViewModel;
    EditText mRecipeName;
    RecipeEntity mRecipeEntity;
    int duration = Toast.LENGTH_SHORT;
    RecipeEntity recipeExisted;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_create_recipe_step1);

        // Initialize the creation of the recipe -> step 1
        Button buttonCreateRecipe = findViewById(R.id.btn_create_recipe);
        buttonCreateRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecipeViewModel = new RecipeViewModel(getApplication());
                // define the item view element where user enters name of recipe
                mRecipeName = findViewById(R.id.recipe_title_edit);
                if (mRecipeName != null){
                    // create new recipe with the user input
                    mRecipeEntity = new RecipeEntity(mRecipeName.getText().toString());
                    recipeExisted = mRecipeViewModel.getRecipeByName(mRecipeName.getText().toString());
                    Log.d("recipe exist", String.valueOf(recipeExisted));

                    //if recipe not exist we create it else we go to the edition
                    if (recipeExisted == null){
                        mRecipeViewModel.insert(mRecipeEntity);
                    }
                    // send to the new intent the name of the recipe created
                    Intent myRecipeCreation = new Intent(MyRecipeCreateActivity.this, MyRecipeUpdateActivity.class);
                    myRecipeCreation.putExtra("recipeName",mRecipeName.getText().toString());
                    startActivity(myRecipeCreation);
                } else {
                    Toast myToastMessage = new Toast(getApplicationContext());
                    myToastMessage.makeText(MyRecipeCreateActivity.this,"Pas d'ingrédient saisi", duration);
                    myToastMessage.show();
                }
            }
        });
    }
}
