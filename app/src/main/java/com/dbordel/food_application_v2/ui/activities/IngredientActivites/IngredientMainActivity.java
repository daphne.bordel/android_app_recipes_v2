package com.dbordel.food_application_v2.ui.activities.IngredientActivites;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.food_application_v2.R;
import com.dbordel.food_application_v2.data.db.model.IngredientEntity;
import com.dbordel.food_application_v2.data.db.model.RecipeEntity;
import com.dbordel.food_application_v2.tools.ingredient.IngredientAdapter;
import com.dbordel.food_application_v2.tools.ingredient.IngredientViewModel;
import com.dbordel.food_application_v2.tools.myRecipe.RecipeViewModel;
import com.dbordel.food_application_v2.ui.activities.MyRecipeActivities.MyRecipeIngredientsUpdate;
import com.dbordel.food_application_v2.ui.activities.MyRecipeActivities.MyRecipeUpdateActivity;

import java.util.List;

public class IngredientMainActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    IngredientAdapter mIngredientAdapter;
    IngredientViewModel mIngredientViewModel;
    List<IngredientEntity> mIngredients;
    Button btnReturnIngredientsRecipe;
    RecipeViewModel mRecipeViewModel;
    RecipeEntity mCurrentRecipe;

    int currentRecipeId;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ingredients_list_activity);

        //get the intent
        Intent intent = getIntent();
        currentRecipeId = intent.getIntExtra("recipeId",0);
        Log.d("current recipe id", String.valueOf(currentRecipeId));
        mRecipeViewModel = new RecipeViewModel(getApplication());
        mCurrentRecipe = mRecipeViewModel.getRecipeById(currentRecipeId);
        Log.d("mCurrentRecipe", mCurrentRecipe.getRecipeName());
        Button btnReturnRecipe = findViewById(R.id.btn_return_recipe_update);
        btnReturnRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IngredientMainActivity.this, MyRecipeUpdateActivity.class);
                intent.putExtra("recipeName",mCurrentRecipe.getRecipeName());
                startActivity(intent);
            }
        });

        // get all ingredients live data
        mIngredientViewModel = new IngredientViewModel(getApplication());
        mIngredientViewModel.getAllIngredients().observe(this, new Observer<List<IngredientEntity>>(){
            // transform LiveData<List<IngredientEntity>> to List<IngredientEntity>
            @Override
            public void onChanged(List<IngredientEntity> ingredientEntities) {
                mIngredients = ingredientEntities;
                // get the recycler view element
                mRecyclerView = findViewById(R.id.recyclerview_ingredients_list);
                // send to adapter
                mIngredientAdapter = new IngredientAdapter(IngredientMainActivity.this,ingredientEntities,getApplication(), currentRecipeId);
                mRecyclerView.setAdapter(mIngredientAdapter);
                mRecyclerView.setLayoutManager(new LinearLayoutManager(IngredientMainActivity.this));
            }
        });

    }

    public void createIngredient(View view){
        Intent createIngredientIntent = new Intent(IngredientMainActivity.this, IngredientsCreateActivity.class);
        createIngredientIntent.putExtra("recipeId",currentRecipeId);
        startActivity(createIngredientIntent);
    }
}
