package com.dbordel.food_application_v2.ui.activities.MyRecipeActivities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.dbordel.food_application_v2.R;
import com.dbordel.food_application_v2.data.db.model.EnumMeasureUnit;
import com.dbordel.food_application_v2.data.db.model.IngredientEntity;
import com.dbordel.food_application_v2.data.db.model.RecipeEntity;
import com.dbordel.food_application_v2.data.db.model.RoomRecipeIngredient;
import com.dbordel.food_application_v2.tools.ingredient.IngredientViewModel;
import com.dbordel.food_application_v2.tools.ingredientsOfRecipe.IngredientsOfRecipeAdapter;
import com.dbordel.food_application_v2.tools.ingredientsOfRecipe.IngredientsOfRecipeViewModel;
import com.dbordel.food_application_v2.tools.myRecipe.RecipeViewModel;
import com.dbordel.food_application_v2.ui.activities.IngredientActivites.IngredientMainActivity;

import java.util.ArrayList;
import java.util.List;

/* -------- CLASS FOR UPDATE THE RECIPE (STEP 2 OF THE RECIPE CREATION TOO) -------------*/

public class MyRecipeUpdateActivity extends AppCompatActivity {

    // view model
    IngredientViewModel mIngredientViewModel;
    RecipeViewModel mRecipeViewModel;
    IngredientsOfRecipeViewModel mIngredientsOfRecipeViewModel;

    List<IngredientEntity> currentIngredients;
    ArrayList<String> mIngredientListName;

    RecipeEntity currentRecipe;
    RoomRecipeIngredient mRoomRecipeIngredient;
    List<RoomRecipeIngredient> mRoomRecipeIngredientTempDelete;

    // variable to store all RoomRecipeIngredient by the recipeId
    // for get all ingredients for one recipe
    LiveData<List<RoomRecipeIngredient>> mGetListOfIngredientsLD;

    IngredientsOfRecipeAdapter mIngredientsOfRecipeAdapter;

    RecyclerView listIngredientRv;
    EditText recipeNameEdit;
    EditText recipeDescriptionEdit;
    EditText preparationTimeEdit;
    EditText cookingTimeEdit;

    String preparationTime;
    String cookingTime;


    @SuppressLint({"LongLogTag", "Range"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_create_recipe_step2);

        //get the name of recipe create to the step 1
        Intent intent = getIntent();
        String stringName = intent.getStringExtra("recipeName");

        //send the name in the second creation activity for show it in the second step
        recipeNameEdit = (EditText) findViewById(R.id.recipe_title_edit);
        recipeNameEdit.setText(stringName);

        //initialization of view model
        mIngredientViewModel = new IngredientViewModel(getApplication());
        mIngredientsOfRecipeViewModel = new IngredientsOfRecipeViewModel(getApplication());
        mRecipeViewModel = new RecipeViewModel(getApplication());

        //get the current recipe to complete his creation (this is a update)
        currentRecipe = mRecipeViewModel.getRecipeByName(recipeNameEdit.getText().toString());
        //get recipe description and show it in the field if exist
        recipeDescriptionEdit = (EditText) findViewById(R.id.edit_recipe_description);

        if (currentRecipe.getRecipeDescription() != null){
            Log.d("current recipe name", currentRecipe.getRecipeDescription());
            recipeDescriptionEdit.setText(currentRecipe.getRecipeDescription());
        }

        //get recipe preparation time and show it in the field if exist
        preparationTimeEdit = (EditText) findViewById(R.id.edit_recipe_preparation_time);
        if (currentRecipe.getRecipeTimePreparation() != null){
            preparationTimeEdit.setText(currentRecipe.getRecipeTimePreparation());
        }

        //get recipe cooking time and show it in the field if exist
        cookingTimeEdit = (EditText) findViewById(R.id.edit_recipe_cooking_time);
        if (currentRecipe.getRecipeTimeCooking() != null){
            cookingTimeEdit.setText(currentRecipe.getRecipeTimeCooking());
        }

        //get all ingredients added for the current recipe
        mGetListOfIngredientsLD = mIngredientsOfRecipeViewModel.getAllRoomRecipeIngredientByRecipeId(currentRecipe.getRecipeId());
        // transform live data list of ingredients for the current recipe to RoomRecipeIngredient entity
        mGetListOfIngredientsLD.observe(MyRecipeUpdateActivity.this, new Observer<List<RoomRecipeIngredient>>(){
            @Override
            public void onChanged(List<RoomRecipeIngredient> roomRecipeIngredients) {
                //recycler view
                listIngredientRv = (RecyclerView) findViewById(R.id.rv_ingredients);
                // Create adapter passing in the sample recipe data
                if (roomRecipeIngredients.size()>0){
                    mIngredientsOfRecipeAdapter = new IngredientsOfRecipeAdapter(MyRecipeUpdateActivity.this, getApplication(), roomRecipeIngredients,R.layout.ingredient_list_for_recipe);
                    // Attach the adapter to the recyclerview to populate items
                    listIngredientRv.setAdapter(mIngredientsOfRecipeAdapter);
                    // Set layout manager to position the items
                    listIngredientRv.setLayoutManager(new LinearLayoutManager(MyRecipeUpdateActivity.this));
                }
            }
        });

        //Show page to manage ingredients of current recipe
        Button buttonManageIngredients = findViewById(R.id.btn_manage_ingredients);
        buttonManageIngredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIngredientsList = new Intent(getApplicationContext(), MyRecipeIngredientsUpdate.class);
                Log.d("before show manage ingredients", String.valueOf(currentRecipe.getRecipeId()));
                myIngredientsList.putExtra("recipeId",currentRecipe.getRecipeId());
                startActivity(myIngredientsList);
            }
        });

        // Finish the creation of a recipe
        Button buttonCreateFinaleRecipe = findViewById(R.id.btn_save_recipe);
        buttonCreateFinaleRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int recipeUpdate;

                // prepare the new intent after save
                Intent myIngredientsListActivity = new Intent(getApplicationContext(), IngredientMainActivity.class);
                startActivity(myIngredientsListActivity);

                // create a new recipe
                RecipeEntity recipe = new RecipeEntity(
                        currentRecipe.getRecipeId(),
                        currentRecipe.getRecipeName(),
                        recipeDescriptionEdit.getText().toString(),
                        preparationTimeEdit.getText().toString(),
                        cookingTimeEdit.getText().toString(),
                        "placeholder.jpg"
                );

                //update recipe created => return 1 if update
                recipeUpdate = mRecipeViewModel.updateRecipe(recipe);

                Intent myRecipeDetails = new Intent(MyRecipeUpdateActivity.this, MyRecipeDetailActivity.class);
                myRecipeDetails.putExtra("recipeId",currentRecipe.getRecipeId());
                if (recipeUpdate == 1){
                    //show details of recipe created
                    startActivity(myRecipeDetails);
                }
            }
        });

        Button btnCancelChangementRecipe = findViewById(R.id.btn_cancel_save_recipe);
        btnCancelChangementRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
