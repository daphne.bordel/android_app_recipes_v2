package com.dbordel.food_application_v2.ui.activities.MyRecipeActivities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.food_application_v2.R;
import com.dbordel.food_application_v2.data.db.RecipeDao;
import com.dbordel.food_application_v2.data.db.model.IngredientEntity;
import com.dbordel.food_application_v2.data.db.model.RecipeEntity;
import com.dbordel.food_application_v2.data.db.model.RoomRecipeIngredient;
import com.dbordel.food_application_v2.tools.myRecipe.MyRecipeAdapter;
import com.dbordel.food_application_v2.tools.myRecipe.RecipeViewModel;

import java.util.List;

public class MyRecipeMainActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    MyRecipeAdapter mMyRecipeAdapter;
    RecipeViewModel mRecipeViewModel;
    int mRecipeId;
    String tvRecipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_recipes_list_activity);

        //A FAIRE SEND THE DATA TO THE RECYCLERVIEW !
        mRecipeViewModel = new RecipeViewModel(getApplication());
        mRecipeViewModel.getAllRecipes().observe(this,new Observer<List<RecipeEntity>>(){
            @Override
            public void onChanged(List<RecipeEntity> recipeEntities) {
                mRecyclerView = findViewById(R.id.recyclerView_myRecipe);
                mMyRecipeAdapter = new MyRecipeAdapter(MyRecipeMainActivity.this, recipeEntities);
                mRecyclerView.setAdapter(mMyRecipeAdapter);
                mRecyclerView.setLayoutManager(new LinearLayoutManager(MyRecipeMainActivity.this));
            }
        });

    }

    public void addRecipe(View view){
        Intent myRecipesIntent = new Intent(MyRecipeMainActivity.this, MyRecipeCreateActivity.class);
        startActivity(myRecipesIntent);
    }

    @SuppressLint("LongLogTag")
    public void onClickRecipe(View view){
        TextView textview = (TextView) view;
        tvRecipe = textview.getText().toString();
        mRecipeId = mRecipeViewModel.getRecipeByName(tvRecipe).getRecipeId();
        Log.d("id of recipe before start intent", String.valueOf(mRecipeId));
        Intent recipeClickedIntent = new Intent(MyRecipeMainActivity.this,MyRecipeDetailActivity.class);
        recipeClickedIntent.putExtra("recipeId",mRecipeId);
        startActivity(recipeClickedIntent);
    }
}
