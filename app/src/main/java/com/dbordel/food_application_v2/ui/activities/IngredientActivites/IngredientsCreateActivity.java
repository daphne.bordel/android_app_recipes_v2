package com.dbordel.food_application_v2.ui.activities.IngredientActivites;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.dbordel.food_application_v2.R;
import com.dbordel.food_application_v2.data.db.model.IngredientEntity;
import com.dbordel.food_application_v2.tools.ingredient.IngredientViewModel;
import com.dbordel.food_application_v2.ui.activities.MyRecipeActivities.MyRecipeIngredientsUpdate;

/* --------- ACTIVITY FOR CREATE AN INGREDIENT -----------*/

public class IngredientsCreateActivity extends AppCompatActivity {
    IngredientViewModel mIngredientViewModel;
    EditText mIngredientName;
    IngredientEntity mIngredientEntity;
    int duration = Toast.LENGTH_SHORT;
    int ingredientId;
    int recipeId;
    TextView titleForm;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_create_ingredient);

        titleForm = findViewById(R.id.title_form_ingredient);
        mIngredientName = findViewById(R.id.edit_ingredient_name);

        Intent intent = getIntent();
        ingredientId = intent.getIntExtra("ingredientId",0);
        Log.d("ingredient id in create activite", String.valueOf(ingredientId));
        recipeId = intent.getIntExtra("recipeId",0);
        Log.d("recipeId in create activity", String.valueOf(recipeId));

        //if ingredient exist we find the ingredient for edit him
        if (ingredientId != 0){
            titleForm.setText("Edition d'un ingrédient :");
            IngredientViewModel ingredientViewModel = new IngredientViewModel(getApplication());
            IngredientEntity currentIngredient = ingredientViewModel.getIngredientById(ingredientId);
            mIngredientName.setText(currentIngredient.getIngredientName());
            // When click on the button "Enregistrer" update ingredient
            Button buttonManageIngredients = findViewById(R.id.btn_save_ingredient);
            buttonManageIngredients.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("LongLogTag")
                @Override
                public void onClick(View v) {
                    mIngredientViewModel = new IngredientViewModel(getApplication());
                    Log.d("mIngredientName", String.valueOf(mIngredientName));
                    // define the EditText element view to retrieve user input and if input is different of initial value
                    if (mIngredientName != null){
                        //changeName of current ingredient and update it
                        currentIngredient.setIngredientName(mIngredientName.getText().toString());
                        Log.d("name changed",mIngredientName.getText().toString());
                        mIngredientViewModel.updateIngredient(currentIngredient);

                        // create new intent for starting new activity who show all ingredients
                        Intent myListOfIngredientIntent = new Intent(IngredientsCreateActivity.this,IngredientMainActivity.class);
                        Log.d("ingredients create recipe Id before main activity", String.valueOf(recipeId));
                        myListOfIngredientIntent.putExtra("recipeId",recipeId);
                        startActivity(myListOfIngredientIntent);

                    }
                }
            });

        } else {
            //if ingredient not exist we created it
            titleForm.setText("Création d'un ingrédient :");
            // When click on the button "Enregistrer" (an ingredient)
            Button buttonManageIngredients = findViewById(R.id.btn_save_ingredient);
            buttonManageIngredients.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mIngredientViewModel = new IngredientViewModel(getApplication());

                    // define the EditText element view to retrieve user input
                    if (mIngredientName != null){

                        // create a new ingredient entity
                        mIngredientEntity = new IngredientEntity(mIngredientName.getText().toString());
                        mIngredientViewModel.insert(mIngredientEntity);

                        // create new intent for starting new activity who show all ingredients
                        Intent myListOfIngredientIntent = new Intent(IngredientsCreateActivity.this,IngredientMainActivity.class);
                        myListOfIngredientIntent.putExtra("recipeId",recipeId);
                        Log.d("ingredient create befor putextra", String.valueOf(recipeId));
                        startActivity(myListOfIngredientIntent);

                    } else {
                        Toast myToastMessage = new Toast(getApplicationContext());
                        myToastMessage.makeText(IngredientsCreateActivity.this,"Pas d'ingrédient saisi", duration);
                        myToastMessage.show();
                    }
                }
            });
        }


        //Button "Annuler"
        // when click on the cancel button we return to the list of ingredients manager
        //return to the manage of ingredients recipe
        Button btnReturnIngredientsManager = findViewById(R.id.btn_cancel_save_ingredient);
        btnReturnIngredientsManager.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                Log.d("Cancel the creation of ingredients","cancel");
                Intent intent = new Intent(IngredientsCreateActivity.this, IngredientMainActivity.class);
                intent.putExtra("recipeId",recipeId);
                startActivity(intent);
            }
        });
    }

}
