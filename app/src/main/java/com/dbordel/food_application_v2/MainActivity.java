package com.dbordel.food_application_v2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.dbordel.food_application_v2.ui.activities.MyRecipeActivities.MyRecipeMainActivity;
import com.dbordel.food_application_v2.ui.activities.SearchRecipeApi.SearchRecipesActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonSuggestionsHome = findViewById(R.id.btn_suggestions_home);
        buttonSuggestionsHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent searchActivityIntent = new Intent(getApplicationContext(), SearchRecipesActivity.class);
                startActivity(searchActivityIntent);
            }
        });

        Button buttonMyRecipesHome = findViewById(R.id.btn_my_recipes_home);
        buttonMyRecipesHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myRecipesActivity = new Intent(getApplicationContext(), MyRecipeMainActivity.class);
                startActivity(myRecipesActivity);
            }
        });
    }
}